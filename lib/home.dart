import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  static const platform = MethodChannel('samples.flutter.dev/battery');


  Future<void> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('battery');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    print(batteryLevel);
    
  }

  void _showToast() {
    platform.invokeMethod('showToast', {"message": "hello toast"});
  }

  void _askPermission() async {
    final String result = await platform.invokeMethod('askPermission');
    print(jsonDecode(result));
  }

  void _getLocation() async {
    final String result = await platform.invokeMethod('getLocation');
    print(jsonDecode(result));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _getBatteryLevel();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              child: Text('Show toast'),
              onPressed: _showToast,
            ),
            TextButton(
              child: Text('Ask permission'),
              onPressed: _askPermission,
            ),
             TextButton(
              child: Text('Get Location'),
              onPressed: _getLocation,
            ),
            
          ],
        ),
      ),
    );
  }
}