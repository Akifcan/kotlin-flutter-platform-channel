package com.example.channel

import android.Manifest
import io.flutter.embedding.android.FlutterActivity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    private val CHANNEL = "samples.flutter.dev/battery"
    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            call, result ->
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

            if(call.method == "battery"){
                val batteryLevel: Int
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                    val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                    batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
                } else {
                    val intent = ContextWrapper(applicationContext).registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
                    batteryLevel = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
                }
                result.success(batteryLevel)
            }
            if(call.method == "showToast"){
                val message : String? = call.argument("message");
                Log.i("TOAST MESSAGE", message.toString());
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            }
            if(call.method == "askPermission"){
                val permission = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        101)

                if(permission == PackageManager.PERMISSION_GRANTED){
                    result.success("{\"result\":  \"true\", \"message\":  \"we have permission\"}")
                }else{
                    result.success("{\"result\":  \"false\", \"message\":  \"couldn't access permission\"}")
                }
            }
            if(call.method == "getLocation"){
                fusedLocationClient.lastLocation
                        .addOnSuccessListener { location : Location? ->
                            result.success("{\"lat\": \"${location?.latitude}\", \"longitude\": \"${location?.longitude}\"  }")
                        }
            }
        }
    }

}
